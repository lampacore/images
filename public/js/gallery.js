$(function() {
    
    $(".sortable-items-container").sortable({

        handle: ".item-drag",
        cursor: "move",
        

        update: function(event, ui) {
          

            var sortedArr = $(".sortable-items-container").sortable("toArray", {
                attribute: "item-id"
            });

            

            $.post("/admin/images/image/set-positions", {order_data: sortedArr.join(',') }, function(answer) {

                
              //alert(answer);

            });


        }


    });
    
    //alert('1');

    $(".file-select-input").on("change", function(e){

      //  alert('1');
       
      $(this).parents('form').submit();
        
        
    });
    
    
    
    $(".image-delete-a").on("click", function(e){
        
        
        e.preventDefault();
        var imageId = $(this).attr('image-id');
        
       $.get("/admin/images/image/delete/"+imageId, function(answer) {
             
             
             
                  $("#image-holder-"+imageId  ).toggle('highlight', {color: "#ffffff"});
                  
                  $(".images_max_alert").slideUp(
                    
                    function(){
                        
                        $(".images_uploader").slideDown();
                        
                    }
                    
                    );
                  
                  

            //  alert(answer);

        });

        
        
    });


    $(".image-desc-a").on("click", function(e){


        e.preventDefault();

        var clickButton = $(this);

        var imageId = $(this).attr('image-id');

        var desc = prompt("Описание:", clickButton.attr('title'));



        $.post("/admin/images/image/desc/"+imageId, {desc: desc}, function(answer) {

            clickButton.attr('title', desc);

            //  alert(answer);

        });



    });
    
    




})
