<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LampacoreImages extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('lampacore_images', function (Blueprint $table) {
            $table->increments('id');
            $table->string('source');
            $table->string('tag')->nullable();
            $table->unsignedInteger('owner_id');
            $table->string('owner_class');
            $table->unsignedInteger('position');

        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::drop('lampacore_images');
    }
}
