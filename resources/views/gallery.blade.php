<?php $owner->flashImagesOwner();
if (!isset($imagesMax)) { $imagesMax = false; }
if (!isset($tag)) { $tag = null; }
if (!isset($viewOnly)) { $viewOnly = false; }
?>
<div class="panel panel-default">

    <div class="panel-heading">

        <div class="row">

            <div class="col-md-6">

                @if(isset($title))
                    {{$title}}
                @else
                    @if($imagesMax == 1)
                        Изображение
                    @elseif($imagesMax > 1)
                        Изображения, максимум: {{$imagesMax}}шт.
                    @else
                        Изображения
                    @endif
                @endif

            </div>

            @if(!$viewOnly  && $owner->images(true)->count()  )
                <div class="col-md-6 text-right">
                    <a href="{{ action('\Lampacore\Images\Controllers\ImageController@getDeleteAllImages', $tag) }}" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span>
                        все</a>
                </div>
            @endif

        </div>

    </div>
    <script type="text/javascript" src="/packages/lampacore/images/js/gallery.js"></script>
    <div class="panel-body">

        @if(!$viewOnly)
            <div class="row col-md-12" style="font-size: 11px; margin-bottom: 5px">

                Допускаются файлы в форматах JPG и PNG.

                Используйте <strong>Ctrl</strong> или <strong>Shift</strong> для выбора нескольких изображений.

            </div>
        @endif

        <div class="row">
            @if($owner->id)

                @if(!$viewOnly)
                    <div class="col-md-3">

                        <div class="row">
                            @if($imagesMax > 1 && $owner->images(true, $tag)->count() >= $imagesMax)
                                <div class="col-md-12 images_max_alert">
                                    <strong>Загружено максимальное количество изображений.</strong>
                                </div>
                            @else
                                <div class="col-md-12 images_uploader">
                                    <form id="image-upload-form" action="{{ action('\Lampacore\Images\Controllers\ImageController@postUpload') }}" method="post" enctype="multipart/form-data">
                                        @if($tag!== null)
                                            <input name="tag" type="hidden" value="{{$tag}}" />
                                        @endif
                                        <input name="images_max" value="{{$imagesMax}}" type="hidden" />

                                        <div style="position: relative; margin-bottom: 5px;">
                                            <input multiple

                                                    @if($imagesMax > 1)
                                                        multiple
                                                        @endif
                                                    accept="image/jpeg,image/png" class="file-select-input" type="file"  name="images[]" style="cursor: default; position: relative; opacity: 0; z-index: 100; width: 180px; height: 34px" />
                                            <button id="file-select-fake-button" class="btn btn-default" style="position: absolute;top: 0px;left: 0px;z-index: 1;">
                                                Загрузить файлы
                                            </button>
                                        </div>
                                    </form>
                                </div>

                            @endif
                        </div>

                    </div>
                @endif

            @else
                <div class="col-md-12">
                    <h2>
                        <small>Для загрузки изображений сначала сохраните.</small>
                    </h2>
                </div>
            @endif

            <div class="col-md-12">
                <div class="row {{!$viewOnly ? 'sortable-items-container' : ''}}" style="margin-top: 40px">

                    @foreach($owner->images(false, $tag) as $image)

                        <div class="text-center col-md-3"
                             id="image-holder-{{$image->id}}" item-id="{{$image->id}}">

                            <a href="{{$image->url()}}" target="_blank" class="box-image" rel="images"><img src="{{$image->url('150x100')}}" class="item-drag img-thumbnail" /></a>
                            @if(!$viewOnly)
                                <p class="text-center" style="margin-top: 3px;">
                                    <a class="btn btn-xs btn-default image-desc-a" href="#" image-id="{{$image->id}}" title="{{$image->desc}}"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a class="btn btn-xs btn-danger image-delete-a" href="#" image-id="{{$image->id}}"><span class="glyphicon glyphicon-trash"></span></a>
                                </p>
                            @endif

                        </div>

                    @endforeach

                </div>
            </div>
        </div>

    </div>

</div>

