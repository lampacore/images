<?php

namespace Lampacore\Images;

use Symfony\Component\HttpFoundation\File\UploadedFile;

trait ImagesTrait
{

    public function images($returnQuery = false, $tag = null)
    {
        $imagesQuery = Image::query();

        $imagesQuery->where('owner_class', $this->getTable())->where('owner_id', $this->id)->positionOrder();

        if ($tag) {
            $imagesQuery->whereTag($tag);
        }

        if ($returnQuery) {
            return $imagesQuery;
        } else {
            return $imagesQuery->get();
        }
    }

    public function firstImage($tag = null)
    {
        $firstImage = $this->images(true, $tag)->first();

        if ($firstImage) {
            return $firstImage;
        } else {
            return new Image;
        }
    }

    public function image($tag = null)
    {
        return $this->firstImage($tag);
    }

    public function attachUploadedImage(UploadedFile $imageFile, $tag = null)
    {
        if (!$this->exists()) {
            throw new \Exception('Cant attach image - object not exists');
        }

        $image = new Image();

        $image->source = strtolower($imageFile->getClientOriginalExtension());

        $image->owner_class = $this->getTable();

        $image->owner_id = $this->id;

        $image->tag = $tag;

        $image->save();

        if (!\File::isDirectory(\Config::get('images.path'))) {
            \File::makeDirectory(\Config::get('images.path'));
        }

        $imageFile->move(\Config::get('images.path'), $image->id . "." . $image->source);

        \Event::fire('lc.image.attach.'.class_basename($this), [ $this->id ]);

        return $image;
    }

    public function deleteAllImages($tag = null)
    {
        $this->images(true, $tag)->delete();
    }

    public function flashImagesOwner()
    {
        \Session::put('images_owner', $this);
    }

}
