<?php

namespace Lampacore\Images;

use Lampacore\Positions\PositionsTrait;

class Image extends \Eloquent
{

    protected $table = 'lampacore_images';

    public $timestamps = false;

    protected $owner;

    use PositionsTrait;

    public function setSourceAttribute($value)
    {
        $value = str_replace([ 'jpeg' ], [ 'jpg' ], $value);
        $this->attributes['source'] = $value;
    }

    public function url($size = false, $dummy = null)
    {

        if (!$size) {
            $webPath = sprintf(\Config::get('images.web_path') . "/%s.%s", $this->id, $this->source);

            return \URL::to($webPath);
        }

        list($w, $h) = explode("x", $size, 2);

        $imagePath = sprintf(\Config::get('images.path') . "/%s.%s", $this->id, $this->source);

        $thumbPath = sprintf(\Config::get('images.path') . "/%s_%sx%s.%s", $this->id, $w, $h, $this->source);

        $webPath = sprintf(\Config::get('images.web_path') . "/%s_%sx%s.%s", $this->id, $w, $h, $this->source);

        if (!$this->id || !file_exists($imagePath)) {
            return $dummy;
        }

        if (!file_exists($thumbPath)) {

            $resizeShellCommand = sprintf('convert -quality 80 -gravity North -resize %s^ -extent %s %s %s', $size,
                $size, $imagePath, $thumbPath);

            shell_exec($resizeShellCommand);

        }

        return \URL::to($webPath);

    }

    public function md5()
    {

        $key = 'img_md5_' . $this->id;

        $imagePath = sprintf(\Config::get('images.path') . "/%s.%s", $this->id, $this->source);

        if ($this->id && file_exists($imagePath)) {

            if (\Cache::has($key)) {
                return \Cache::get($key);
            } else {
                \Cache::put($key, md5_file($imagePath), 10080);
            }

            return \Cache::get($key);

        } else {
            return null;
        }

    }

}
