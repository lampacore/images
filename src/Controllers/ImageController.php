<?php

namespace Lampacore\Images\Controllers;

use Illuminate\Routing\Controller;
use Lampacore\Images\Image;
use Lampacore\Images\ImagesTrait;

class ImageController extends Controller
{

    /**
     * @type ImagesTrait
     */
    protected $owner;

    public function __construct()
    {
        $this->owner = \Session::get('images_owner');
        \Session::reflash();
    }

    public function getDeleteAllImages($tag = null)
    {

        $this->owner->deleteAllImages($tag);

        return \Redirect::back()->with('msg', 'msg.images.image.all_deleted');

    }

    public function postUpload()
    {

        $imagesMax = \Input::get('images_max');
        $imagesCount = $this->owner->images(true, \Input::get('tag'))->count();

        if ($imagesMax == 1) {
            $this->owner->deleteAllImages(\Input::get('tag'));
        } elseif ($imagesMax && $imagesCount >= $imagesMax) {
            return \Redirect::back()->with('msg', 'msg.images.image.upload_limit');
        }

        foreach (\Input::file('images') as $c => $imageFile) {

            if ($imagesMax > 1 && ($imagesCount + $c + 1) > $imagesMax) {
                break;
            }

            $this->owner->attachUploadedImage($imageFile, \Input::get('tag'));

        }

        return \Redirect::back()->with('msg', 'msg.images.image.upload');

    }

    public function getDelete($imageID)
    {
        $image = Image::findOrFail($imageID);
        $image->delete();

        return '';
    }

    public function postDesc($imageID)
    {
        $image = Image::findOrFail($imageID);
        $image->desc = \Request::input('desc');
        $image->save();
        
        return '1';
    }


    public function postSetPositions()
    {

        $ids = explode(',', \Input::get('order_data'));
        Image::setPositions($ids);

        return '';
    }

}
