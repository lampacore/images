<?php namespace Lampacore\Images;

use Illuminate\Support\ServiceProvider;

class LampacoreImagesServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     * @type bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     * @return void
     */
    public function boot()
    {
        //$this->package('lampacore/images', 'images', base_path('workbench/lampacore/images/resources'));

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'images');

        $this->publishes([__DIR__.'/../migrations' => database_path('migrations')], 'migrations');

        $this->publishes([__DIR__.'/../public' => public_path('packages/lampacore/images')], 'assets');

        include __DIR__ . '/routes.php';

    }

    /**
     * Register the service provider.
     * @return void
     */
    public function register()
    {

    }

    /**
     * Get the services provided by the provider.
     * @return array
     */
    public function provides()
    {
        return [ ];
    }

}
